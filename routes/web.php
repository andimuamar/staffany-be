<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});


$router->get('/shifts', 'ShiftsController@show');
$router->get('/shifts/{id}', 'ShiftsController@detail');
$router->post('/shifts/create', 'ShiftsController@create');
$router->post('/shifts/update','ShiftsController@update');
$router->delete('/shifts/delete/{id}','ShiftsController@destroy');