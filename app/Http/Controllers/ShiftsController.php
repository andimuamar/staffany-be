<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Shifts;


class ShiftsController extends Controller
{
    public function show(){
        $data = Shifts::All();
        return response()->json($data);
    }

    public function detail($id){
        $data = Shifts::find($id);
        return response()->json($data);
    }

    public function create(Request $request)
    {
        $this->validate($request, [
            "name" => "required",
            "date" => "required",
            "start_time" => "required",
            "end_time" => "required"

        ]);
        
        $check_date = $this->checkDate($request->date);
        $check_name = $this->checkName($request->name);

        if(count($check_date) > 0 && count($check_name) > 0){
            return response()->json([
                'success' => false,
                'message' => 'Cannot, date has taken'
            ]);
        }else{

            $shift = new Shifts();
            $shift->name = $request->name;
            $shift->date = $request->date;
            $shift->start_time = $request->start_time;
            $shift->end_time = $request->end_time;
            $shift->status = "publish";
            $shift->save();

            return response()->json([
                'success' => true,
                'message' => 'Success'
            ], 200);
        }


    }

    public function update(Request $request)
    {

        $shift = Shifts::whereId($request->id)->update([
            'name'     => $request->name,
            'date'   => $request->date,
            'start_time'   => $request->start_time,
            'end_time'   => $request->end_time
        ]);

        if ($shift) {
            return response()->json([
                'success' => true,
                'message' => 'Success Update',
                'data' => $shift
            ], 200);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Update Fail',
            ], 400);
        }

        return response()->json($shift);
    }

    public function destroy($id)
    {
        $shift = Shifts::find($id);
        
        if (!$shift) {
            return response()->json(['message' => 'Data not found'], 404);
        }

        if($shift->status != "published"){
            $shift->delete();
            return response()->json(['message' => 'Data deleted successfully'], 200);
        }
        else{
            return response()->json(['message' => 'Shift is published cannot delete'], 200);
        }

    }

    public function publishShift(){

    }

    protected function checkdate($date){
        $shift = Shifts::where('date', $date)->get();

        return $shift;
    }

    protected function checkName($name){
        $shift = Shifts::where('name', $name)->get();

        return $shift;
    }

    protected function weekOfMonth($date) {
        //Get the first day of the month.
        $firstOfMonth = strtotime(date("Y-m-01", $date));
        //Apply above formula.
        return $this->weekOfYear($date) - $this->weekOfYear($firstOfMonth) + 1;
    }

   

}
