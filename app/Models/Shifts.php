<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Shifts extends Model
{
    protected $table = 'shifts';

    protected $fillable = [
        'name', 'date', 'start_time', 'end_time', 'status'
    ];

    
}
