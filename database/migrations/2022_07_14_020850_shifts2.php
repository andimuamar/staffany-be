<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
   
    public function up() {
        Schema::create('shifts', function (Blueprint $table) {
            
            $table->increments('id');
            $table->string('name', 100);
            $table->string('date', 100);
            $table->string('start_time', 100);
            $table->string('end_time', 100);
            $table->string('status', 100);
            $table->timestamps();
        });
    }

    public function down() {
        Schema::dropIfExists('shifts');
    }
};
